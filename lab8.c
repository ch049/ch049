#include<stdio.h>
#include<conio.h>
int main()
{
    int i, num, pos, arr[10], n;
    printf("\nenter the size of the array:");
    scanf("%d", &n);
    printf("\nenter the elements of the array:");
    for(i=0;i<n;i++)
    scanf("%d", &arr[i]);
    printf("\n enter the position from which the number has to be deleted:");
    scanf("%d", &pos);
    for(i=pos;i<n;i++)
    arr[i]=arr[i+1];
    n--;
    printf("\narray after deletion is:");
    for(i=0;i<n;i++)
    printf("\nArr[%d]=%d", i, arr[i]);
    getch();
    return 0;
}    