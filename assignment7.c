#include <stdio.h>

void pattern(int n, int m)

{

    int i, j;

    for (i = 1; i <= n; i++)

    {

        for (j = 1; j <= m; j++)

        {

            printf("@");

        }

        printf("\n");

    }

}

int main()

{

    int rows =3, columns =4;

    printf("\n");

    pattern(rows, columns);

    return 0;

}
