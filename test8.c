#include<stdio.h>
#include<conio.h>
int main(){
    struct date{
        int day;
        int month;
        int year;
    }; 
    struct date joining_date;
    struct employee{
        int ID;
        char name[100];
        int salary;
        struct date joining_date;
    }; struct employee emp;
    printf("\n Enter the name of the employee:");
    scanf("%s", emp.name);
    printf("\n Enter the ID no of the employee:");
    scanf("%d", &emp.ID);
    printf("\n Enter the salary of the employee:");
    scanf("%d", &emp.salary);
    printf("\n Enter the joining date of the employee:");
    scanf("%d%d%d", &emp.joining_date.day, &emp.joining_date.month, &emp.joining_date.year);
    printf("\n ******EMPLOYEE DETAILS*******\n");
    printf("\n NAME: %s", emp.name);
    printf("\n ID_NO: %d", emp.ID);
    printf("\n SALARY: %d", emp.salary);
    printf("\n JOINING DATE: %d-%d-%d", emp.joining_date.day, emp.joining_date.month, emp.joining_date.year);
    return 0;
}
